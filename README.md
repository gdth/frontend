## GDTH Frontend
* Proyecto desarrollado con el Framework de Javascript  VueJs 2.0
* Para el desarrollo de la aplicación se utiliza Vue Material Admin Template.

## Requerimientos
* NodeJs v10.0.0 o superior
* Yarn v1.10 o superior
* Navegador Google Chrome, firefox, opera, each O Internet explorer mayor a la versión 10.
* Extensión VueTools
* Vue clie 3.x.x
* Permisos de administrador en el equipo Pc para el desarrollo.

## Comandos para la instalación, configuración y pruebas del proyecto
*  Instalación.- yarn install
* Compilar proyecto para desarollo.- yarn run serve
* Compilar proyecto para producción.- yarn run build

## Estructura del Proyecto
``` bash
├── build
├── config (Webpack)
├── src
│   ├── api
│   ├── components
│   ├── mixins
│   ├── views (or views)
│   ├── router
│   ├── util
│   ├── theme
│   │   ├── default.styl
│   └── App.vue
│   └── event.js
│   └── main.js
├── dist
├── release
├── static (or asset)
├── mock (or script to build mock data)
├── node_modules
├── test
├── README.md
├── package.json
├── index.html
└── .gitignore
```